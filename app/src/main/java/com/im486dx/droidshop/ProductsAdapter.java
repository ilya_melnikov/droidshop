package com.im486dx.droidshop;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.im486dx.droidshop.model.Product;
import com.im486dx.droidshop.model.ProductDao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by nightrain on 8/4/15.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    private static ProductsAdapter mInstance;
    private List<Product> mDataset = new ArrayList<>();
    private Locale mLocale;
    private int mSelectedItemIndex = -1;
    private int mSelectedItemColor;
    private int mNormalItemColor;
    private int mSelectedTextColor;
    private int mNormalTextColor;

    private ProductsAdapter() {
        super();
        String languageToLoad  = "ru";
        String countryToLoad = "RU";
        mLocale = new Locale(languageToLoad, countryToLoad);

    }

    public static void init(DaoActivity activity) {
        if (mInstance != null) return;
        mInstance = new ProductsAdapter();
        mDataLoaderTask.execute(activity);
        mInstance.mSelectedItemColor = activity.getResources().getColor(R.color.accent_color);
        mInstance.mNormalItemColor = activity.getResources().getColor(R.color.background_color);
        mInstance.mNormalTextColor = activity.getResources().getColor(R.color.primary_text_color);
        mInstance.mSelectedTextColor = activity.getResources().getColor(R.color.background_color);
    }

    public static ProductsAdapter getInstance() {
        if (mInstance == null) {
            mInstance = new ProductsAdapter();
        }
        return mInstance;
    }

    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ViewHolder holder, final int position) {
        holder.root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final Product item = mDataset.get(position);
        holder.title.setText(item.getTitle());
        holder.price.setText(DecimalFormat.getCurrencyInstance(mLocale).format(item.getPrice()));
        holder.qty.setText(Integer.toString(item.getQty()));
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Broadcast.send(v.getContext(), Broadcast.ACTION_ITEM_SELECTED, item.getId());
            }
        });
        if (mSelectedItemIndex == position) {
            holder.root.setBackgroundColor(mSelectedItemColor);
            holder.title.setTextColor(mSelectedTextColor);
            holder.price.setTextColor(mSelectedTextColor);
            holder.qty.setTextColor(mSelectedTextColor);
        } else {
            holder.root.setBackgroundColor(mNormalItemColor);
            holder.title.setTextColor(mNormalTextColor);
            holder.price.setTextColor(mNormalTextColor);
            holder.qty.setTextColor(mNormalTextColor);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View root;
        TextView title;
        TextView price;
        TextView qty;

        public ViewHolder(View root) {
            super(root);
            this.root = root;
            title = (TextView) root.findViewById(R.id.title);
            price = (TextView) root.findViewById(R.id.price);
            qty = (TextView) root.findViewById(R.id.qty);
        }
    }

    private int indexOf(long productId) {
        for (int i = 0; i < mDataset.size(); i++) {
            Product item = mDataset.get(i);
            if (item.getId() == productId) return i;
        }
        return -1;
    }

    public void itemChanged(Product product) {
        for (int i = 0; i < mDataset.size(); i++) {
            Product item = mDataset.get(i);
            if (item.getId() == product.getId()) {
                item.setTitle(product.getTitle());
                item.setDescription(product.getDescription());
                item.setPrice(product.getPrice());
                item.setQty(product.getQty());
                notifyItemChanged(i);
                return;
            }
        }
        mDataset.add(product);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void itemRemoved(long productId) {
        int index = indexOf(productId);
        if (index < 0) return;
        mDataset.remove(index);
        if (mSelectedItemIndex == index) {
            mSelectedItemIndex = -1;
        }
        notifyItemRemoved(index);
    }

    public int getSelectedItemIndex() {
        return mSelectedItemIndex;
    }

    public void selectItem(long productId) {
        int index = indexOf(productId);
        if (index < 0) return;
        int oldSelected = mSelectedItemIndex;
        mSelectedItemIndex = index;
        notifyItemChanged(oldSelected);
        notifyItemChanged(mSelectedItemIndex);
    }

    private static List<Product> createTestItems(Context context, ProductDao productDao) {
        try {
            Resources res = context.getResources();
            InputStream in = res.openRawResource(R.raw.products);
            byte[] b = new byte[in.available()];
            in.read(b);
            in.close();
            String json =  new String(b);
            JSONArray productsArray = new JSONArray(json);
            List<Product> result = new ArrayList<Product>(productsArray.length());
            for (int i = 0; i < productsArray.length(); i++) {
                JSONObject productObject = productsArray.getJSONObject(i);
                Product product = new Product();
                product.setTitle(productObject.getString("title"));
                product.setDescription(productObject.getString("description"));
                product.setPrice(productObject.getDouble("price"));
                product.setQty(productObject.getInt("qty"));
                result.add(product);
            }
            productDao.insertInTx(result);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static AsyncTask<DaoActivity, Void, List<Product>> mDataLoaderTask = new AsyncTask<DaoActivity, Void, List<Product>>() {

        @Override
        protected List<Product> doInBackground(DaoActivity... params) {
            DaoActivity activity = params[0];
            ProductDao productDao = activity.getSession().getProductDao();

            List<Product> products = productDao.loadAll();
            if ((products == null) || (products.size() == 0)) {
                products = createTestItems(activity, productDao);
            }
            return products;
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            mInstance.mDataset = products;
            mInstance.notifyDataSetChanged();
        }
    };

}
