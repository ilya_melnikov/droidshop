package com.im486dx.droidshop;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public class ItemPickerDialogFragment extends DialogFragment {
    public static final String LOGTAG = "PickerDialogFragment";
    private static final String ARG_PRODUCT_ID = "ARG_PRODUCT_ID";
    private static final String ARG_ITEMS = "ARG_ITEMS";

    private ArrayList<Item> mItems;
    private long mProductId;

    public static class Item {
        private String title;
        private int intValue;
        private String stringValue;

        private static final String KEY_TITLE        = "title";
        private static final String KEY_INT_VALUE    = "intValue";
        private static final String KEY_STRING_VALUE = "stringValue";

        public Item(String title, int value) {
            assert(!TextUtils.isEmpty(title));

            this.title = title;
            this.intValue = value;
        }

        public Item(String title, String value) {
            assert(!TextUtils.isEmpty(title));

            this.title = title;
            this.stringValue = value;
        }

        public Item(Bundle bundle) {
            title = bundle.getString(KEY_TITLE, null);
            intValue = bundle.getInt(KEY_INT_VALUE, 0);
            stringValue = bundle.getString(KEY_STRING_VALUE, null);
        }

        public Bundle getValuesBundle() {
            Bundle bundle = new Bundle();

            bundle.putString(KEY_TITLE, title);
            bundle.putInt(KEY_INT_VALUE, intValue);
            if (stringValue != null) {
                bundle.putString(KEY_STRING_VALUE, stringValue);
            }

            return bundle;
        }

        public String getTitle() {
            return title;
        }

        public int getIntValue() {
            return intValue;
        }

        public String getStringValue() {
            return stringValue;
        }

        public static Bundle bundleOfItems(List<Item> items) {
            int itemCount = items.size();
            ArrayList<Bundle> itemBundles = new ArrayList<>();
            for (int i = 0; i < itemCount; ++i) {
                itemBundles.add(items.get(i).getValuesBundle());
            }

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(ARG_ITEMS, itemBundles);
            return bundle;
        }

        public static ArrayList<Item> itemsFromBundle(Bundle bundle) {
            ArrayList<Bundle> itemBundles = bundle.getParcelableArrayList(ARG_ITEMS);
            ArrayList<Item> items = new ArrayList<>();
            for (Bundle itemBundle: itemBundles) {
                items.add(new Item(itemBundle));
            }
            return items;
        }
    }

    public static ItemPickerDialogFragment newInstance(long productId, String[] productActions) {
        ArrayList<ItemPickerDialogFragment.Item> pickerItems = new ArrayList<>();
        pickerItems.add(new ItemPickerDialogFragment.Item(productActions[0], Broadcast.ACTION_ITEM_EDIT));
        pickerItems.add(new ItemPickerDialogFragment.Item(productActions[1], Broadcast.ACTION_ITEM_DELETE));

        Bundle args = new Bundle();
        args.putLong(ARG_PRODUCT_ID, productId);
        args.putBundle(ARG_ITEMS, Item.bundleOfItems(pickerItems));

        ItemPickerDialogFragment fragment = new ItemPickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public ItemPickerDialogFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ARG_PRODUCT_ID, mProductId);
        outState.putBundle(ARG_ITEMS, Item.bundleOfItems(mItems));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null) {
            mItems = Item.itemsFromBundle(args.getBundle(ARG_ITEMS));
            mProductId = args.getLong(ARG_PRODUCT_ID, -1);
        }

        if (savedInstanceState != null) {
            mProductId = savedInstanceState.getLong(ARG_PRODUCT_ID, mProductId);
            mItems = Item.itemsFromBundle(savedInstanceState.getBundle(ARG_ITEMS));
        }

        final String[] itemTitles = getItemTitlesArray();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(itemTitles, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Broadcast.send(getActivity(), mItems.get(which).getStringValue(), mProductId);
            }
        });

        return builder.create();
    }

    private String[] getItemTitlesArray() {
        final int itemCount = mItems.size();
        String[] itemTitles = new String[itemCount];
        for (int i = 0; i < itemCount; ++i) {
            itemTitles[i] = mItems.get(i).getTitle();
        }
        return itemTitles;
    }
}

