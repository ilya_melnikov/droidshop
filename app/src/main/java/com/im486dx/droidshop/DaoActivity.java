package com.im486dx.droidshop;

import android.support.v7.app.AppCompatActivity;

import com.im486dx.droidshop.model.DaoSession;

/**
 * Created by nightrain on 8/4/15.
 */
public abstract class DaoActivity extends AppCompatActivity {
    abstract DaoSession getSession();
    abstract void setActiveProduct(long productId);
}
