package com.im486dx.droidshop;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;

import com.im486dx.droidshop.model.Product;

/**
 * Created by nightrain on 8/8/15.
 */
public class Receiver extends BroadcastReceiver {
    private DaoActivity mActivity;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null) return;
        long productId = intent.getLongExtra(Broadcast.KEY_PRODUCT_ID, 0);
        if (productId < 0) return;
        switch(action) {
            case Broadcast.ACTION_ITEM_SELECTED:
                onItemSelected(productId);
                break;
            case Broadcast.ACTION_ITEM_CHANGED:
                mActivity.removeStickyBroadcast(intent);
                onItemChanged(productId);
                break;
            case Broadcast.ACTION_ITEM_EDIT:
                onItemEdit(productId);
                break;
            case Broadcast.ACTION_ITEM_DELETE:
                onItemDelete(productId);
                break;
            case Broadcast.ACTION_ITEM_CREATE:
                onItemEdit(0);
                break;
        }
    }

    public void register(DaoActivity activity) {
        mActivity = activity;
        IntentFilter filter = new IntentFilter();
        filter.addAction(Broadcast.ACTION_ITEM_SELECTED);
        filter.addAction(Broadcast.ACTION_ITEM_CHANGED);
        filter.addAction(Broadcast.ACTION_ITEM_EDIT);
        filter.addAction(Broadcast.ACTION_ITEM_DELETE);
        filter.addAction(Broadcast.ACTION_ITEM_CREATE);
        activity.registerReceiver(this, filter);
    }

    public void unregister() {
        mActivity.unregisterReceiver(this);
        mActivity = null;
    }

    private void onItemSelected(long productId) {
        String[] productActions = mActivity.getResources().getStringArray(R.array.product_actions);
        ItemPickerDialogFragment dialog = ItemPickerDialogFragment.newInstance(
                productId,
                productActions
        );
        dialog.show(mActivity.getSupportFragmentManager(), ItemPickerDialogFragment.LOGTAG);
    }

    private void onItemChanged(long productId) {
        Product product = mActivity.getSession().getProductDao().load(productId);
        if (product != null) {
            ProductsAdapter.getInstance().itemChanged(product);
        }
        mActivity.setActiveProduct(0);
        mActivity.setTitle(R.string.product_list);
        setHomeEnabled(false);
    }

    private void onItemEdit(long productId) {
        DetailsFragment detailsFragment = DetailsFragment.newInstance(productId);
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        if (mActivity.findViewById(R.id.details) == null) {
            transaction.add(R.id.productsList, detailsFragment);
            transaction.addToBackStack(null);
            setHomeEnabled(true);
            mActivity.setTitle(R.string.product_details);
        } else {
            transaction.replace(R.id.details, detailsFragment);
            ProductsAdapter.getInstance().selectItem(productId);
        }
        mActivity.setActiveProduct(productId);
        transaction.commit();
    }

    private void onItemDelete(long productId) {
        mActivity.getSession().getProductDao().deleteByKey(productId);
        ProductsAdapter adapter = ProductsAdapter.getInstance();
        adapter.itemRemoved(productId);
        if (mActivity.findViewById(R.id.details) != null) {
            if (adapter.getSelectedItemIndex() == -1) {
                FragmentManager manager = mActivity.getSupportFragmentManager();
                Fragment fragment = manager.findFragmentById(R.id.details);
                if (fragment != null) {
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.remove(fragment);
                    transaction.commit();
                }
            }
        }
    }

    private void setHomeEnabled(boolean enabled) {
        ActionBar actionBar = mActivity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(enabled);
        }
    }


}
