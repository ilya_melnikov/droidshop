package com.im486dx.droidshop;

import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


public class ProductsListFragment extends Fragment implements View.OnClickListener {
    private RecyclerView mList;
    private FloatingActionButton mFab;
    private RecyclerView.LayoutManager mLayoutManager;

    public ProductsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        mList = (RecyclerView) root.findViewById(R.id.list);
        mList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(mLayoutManager);
        mList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        mList.setItemAnimator(new DefaultItemAnimator());
        mList.setAdapter(ProductsAdapter.getInstance());
        mList.setOnScrollListener(new ScrollListener());
        mFab = (FloatingActionButton) root.findViewById(R.id.fab);
        mFab.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        Broadcast.send(getActivity(), Broadcast.ACTION_ITEM_CREATE, 0);
    }

    class ScrollListener extends RecyclerView.OnScrollListener {
        int scrollDist = 0;
        boolean isVisible = true;
        private static final int MIN_SCROLL = 4;
        private static final int FAB_MARGIN = 24;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (isVisible && scrollDist > MIN_SCROLL) {
                hide();
                scrollDist = 0;
                isVisible = false;
            }
            else if (!isVisible && scrollDist < -MIN_SCROLL) {
                show();
                scrollDist = 0;
                isVisible = true;
            }
            if ((isVisible && dy > 0) || (!isVisible && dy < 0)) {
                scrollDist += dy;
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        public void show() {
            mFab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
        }

        public void hide() {
            mFab.animate().translationY(mFab.getHeight() + FAB_MARGIN).setInterpolator(new AccelerateInterpolator(2)).start();
        }
    }


}
