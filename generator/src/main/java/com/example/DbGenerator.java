package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DbGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1000, "com.im486dx.droidshop.model");

        addNote(schema);

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }

    private static void addNote(Schema schema) {
        Entity note = schema.addEntity("Product");
        note.addIdProperty();
        note.addStringProperty("title").notNull();
        note.addStringProperty("description").notNull();
        note.addDoubleProperty("price");
        note.addIntProperty("qty");
    }
}
