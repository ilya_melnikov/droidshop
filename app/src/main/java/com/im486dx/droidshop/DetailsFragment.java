package com.im486dx.droidshop;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.im486dx.droidshop.model.Product;
import com.im486dx.droidshop.model.ProductDao;


public class DetailsFragment extends Fragment {
    private long mProductId;
    private Product mProduct;
    private EditText mTitleView;
    private EditText mDescriptionView;
    private EditText mPriceView;
    private EditText mQtyView;

    public static DetailsFragment newInstance(long productId) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putLong(Broadcast.KEY_PRODUCT_ID, productId);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductId = getArguments().getLong(Broadcast.KEY_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_details, container, false);
        mTitleView = (EditText) rootView.findViewById(R.id.title);
        mDescriptionView = (EditText) rootView.findViewById(R.id.description);
        mPriceView = (EditText) rootView.findViewById(R.id.price);
        mQtyView = (EditText) rootView.findViewById(R.id.qty);
        init();
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return rootView;
    }

    private void init() {
        ProductDao dao = ((DaoActivity) getActivity()).getSession().getProductDao();
        mProduct = dao.load(mProductId);
        if (mProduct == null) {
            mProduct = new Product();
        }
        mTitleView.setText(mProduct.getTitle());
        mDescriptionView.setText(mProduct.getDescription());
        mPriceView.setText(Double.toString(mProduct.getPrice()));
        mQtyView.setText(Long.toString(mProduct.getQty()));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mTitleView.getText().toString().equals("")
                && mDescriptionView.getText().toString().equals("")) return;

        ProductDao dao = ((DaoActivity) getActivity()).getSession().getProductDao();
        Product product = dao.load(mProductId);
        if (product == null) {
            if (mProductId != 0) {
                return; //deleted from DB
            }
        }
        try {
            mProduct.setTitle(mTitleView.getText().toString());
            mProduct.setDescription(mDescriptionView.getText().toString());
            mProduct.setPrice(Double.parseDouble(mPriceView.getText().toString()));
            mProduct.setQty(Integer.parseInt(mQtyView.getText().toString()));
            mProductId = dao.insertOrReplace(mProduct);
            mProduct.setId(mProductId);
            Broadcast.sendSticky(getActivity(), Broadcast.ACTION_ITEM_CHANGED, mProductId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

}
