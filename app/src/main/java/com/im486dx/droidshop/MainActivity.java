package com.im486dx.droidshop;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.im486dx.droidshop.model.DaoMaster;
import com.im486dx.droidshop.model.DaoSession;


public class MainActivity extends DaoActivity {
    private static final String DB_NAME = "droidshop";
    private DaoSession mDaoSession;
    FragmentManager mFragmentManager;
    Receiver mReceiver = new Receiver();
    private long mActiveProductId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mActiveProductId = savedInstanceState.getLong(Broadcast.KEY_PRODUCT_ID);
        }
        initDb();
        setContentView(R.layout.activity_main);
        setupToolbar();
        setTitle(R.string.product_list);
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.productsList, new ProductsListFragment());
        transaction.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        mReceiver.register(this);
        if (mActiveProductId != 0) {
            Log.d("test", "mActiveProductId " + mActiveProductId);
            Broadcast.send(this, Broadcast.ACTION_ITEM_EDIT, mActiveProductId);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mReceiver.unregister();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            this.onBackPressed();
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            setTitle(R.string.product_list);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDb() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, DB_NAME, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        mDaoSession = daoMaster.newSession();
        ProductsAdapter.init(this);
    }

    @Override
    public DaoSession getSession() {
        return mDaoSession;
    }

    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.background_color));
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putLong(Broadcast.KEY_PRODUCT_ID, mActiveProductId);
        super.onSaveInstanceState(state);
    }

    @Override
    public void setActiveProduct(long productId) {
        mActiveProductId = productId;
    }
}
