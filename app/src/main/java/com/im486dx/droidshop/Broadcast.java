package com.im486dx.droidshop;

import android.content.Context;
import android.content.Intent;

/**
 * Created by nightrain on 8/6/15.
 */
public class Broadcast {
    public static final String ACTION_ITEM_SELECTED = "com.im486dx.droidshop.ACTION_ITEM_SELECTED";
    public static final String ACTION_ITEM_CHANGED = "com.im486dx.droidshop.ACTION_ITEM_CHANGED";
    public static final String ACTION_ITEM_EDIT = "com.im486dx.droidshop.ACTION_ITEM_EDIT";
    public static final String ACTION_ITEM_DELETE = "com.im486dx.droidshop.ACTION_ITEM_DELETE";
    public static final String ACTION_ITEM_CREATE = "com.im486dx.droidshop.ACTION_ITEM_CREATE";

    public static final String KEY_PRODUCT_ID = "KEY_PRODUCT_ID";

    private static void send(Context context, String action, long productId, boolean isSticky) {
        Intent intent = new Intent(action);
        intent.putExtra(KEY_PRODUCT_ID, productId);
        if (isSticky) {
            context.sendStickyBroadcast(intent);
        } else {
            context.sendBroadcast(intent);
        }
    }

    public static void send(Context context, String action, long productId) {
        send(context, action, productId, false);
    }

    public static void sendSticky(Context context, String action, long productId) {
        send(context, action, productId, true);
    }
}
